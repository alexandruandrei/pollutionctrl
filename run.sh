echo Setting environment variables...
export PI_TOOLS=/usr
export JAVA_HOME=/usr/lib/jvm/jdk-8-oracle-arm32-vfp-hflt
export MYAPPPATH=/home/pi/Desktop/test/PollutionCtrlRPI
export PATH=.:$JAVA_HOME/bin:$PATH

echo Compilling classes ...
cd $MYAPPPATH/src
javac -classpath .:../libs/dio/dio.jar -d ../build/classes com/oracle/dio/gpio/*.java
javac -classpath .:../libs/dio/dio.jar -d ../build/classes com/oracle/dio/spi/*.java
javac -classpath .:../libs/dio/dio.jar -d ../build/classes com/oracle/dio/ProgMainDioSpiGpio.java

echo Creating archive ...
cd $MYAPPPATH/build/classes
jar -cvf $MYAPPPATH/dist/dio-adc_spi-gpio-mqtt-coap.jar com/oracle/* mqtt_paho_eclipse_mosquitto/* jcoap/* -C ../../config/ META-INF

echo Running ...
cd $MYAPPPATH/dist
sudo -E java -Djava.security.policy=../config/dio.policy
-classpath .:../libs/dio/dio.jar:./dio-adc_spi-gpio-mqtt-coap.jar
-Djava.library.path=.:../libs/dio/
-Djdk.dio.registry=../config/dio.properties-raspberrypi
-Ddeviceacess.uart.prefix=/dev/ttyAMA com.oracle.dio.ProgMainDioSpiGpio 300 1 5 21
