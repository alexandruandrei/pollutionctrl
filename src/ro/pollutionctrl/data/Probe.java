package ro.pollutionctrl.data;

import java.util.Date;
import java.util.List;

/**
 * Created by Andrei on 24-Mar-18.
 * Probe containing sensor measurements
 */
public class Probe {

    private int id;

    //Station data
    private int stationId;
    private String stationIdentifier;
    private Position position;

    //Sensor data
    private double temperature;
    private double pressure;
    private double altitude;
    private double humidity;
    private double co2_ppm;
    private double nh4_ppm;
    private double toluene_ppm;
    private double co_ppm;
    private double ch4_ppm;
    private double db;

    private Date registeredAt;

    public Probe() {}

    public Probe(String stationIdentifier, Position position, double temperature,
                 double pressure, double altitude, double humidity, double co2_ppm,
                 double nh4_ppm, double toluene_ppm, double co_ppm, double ch4_ppm) {
        this.stationIdentifier = stationIdentifier;
        this.position = position;
        this.temperature = temperature;
        this.pressure = pressure;
        this.altitude = altitude;
        this.humidity = humidity;
        this.co2_ppm = co2_ppm;
        this.nh4_ppm = nh4_ppm;
        this.toluene_ppm = toluene_ppm;
        this.co_ppm = co_ppm;
        this.ch4_ppm = ch4_ppm;
    }

    public Probe(int id, int stationId, String stationIdentifier, Position position,
                 double temperature, double pressure, double altitude, double humidity,
                 double co2_ppm, double nh4_ppm, double toluene_ppm, double co_ppm, double ch4_ppm) {
        this.id = id;
        this.stationId = stationId;
        this.stationIdentifier = stationIdentifier;
        this.position = position;
        this.temperature = temperature;
        this.pressure = pressure;
        this.altitude = altitude;
        this.humidity = humidity;
        this.co2_ppm = co2_ppm;
        this.nh4_ppm = nh4_ppm;
        this.toluene_ppm = toluene_ppm;
        this.co_ppm = co_ppm;
        this.ch4_ppm = ch4_ppm;
    }

    private Probe(Builder builder){
        this.id = builder.id;
        this.stationId = builder.stationId;
        this.position = builder.position;
        this.temperature = builder.temperature;
        this.pressure = builder.pressure;
        this.altitude = builder.altitude;
        this.humidity = builder.humidity;
        this.co2_ppm = builder.co2_ppm;
        this.nh4_ppm = builder.nh4_ppm;
        this.toluene_ppm = builder.toluene_ppm;
        this.co_ppm = builder.co_ppm;
        this.ch4_ppm = builder.ch4_ppm;
        this.registeredAt = builder.registeredAt;
        this.db = builder.db;
    }

    /**
     * Calculates average probe of a probes list.
     * The returned probe does not have station details/position assigned!
     * @param probeList probe list
     * @return Average Probe
     */
    public static Probe averageProbe(List<Probe> probeList){
        if(probeList == null || probeList.isEmpty()){
            System.out.println("Could not get the avg probe. ProbeList is null/empty");
            return null;
        }

        double avgAltitude = 0, avgHumidity = 0, avgCo2PPM = 0, avgNh4PPM = 0, avgToluenePPM = 0,
                avgCoPPM = 0, avgCh4PPM = 0, avgPressure = 0, avgTemperature = 0, avgDb = 0;

        for(Probe probe:probeList){
            avgTemperature += probe.getTemperature();
            avgPressure += probe.getPressure();
            avgAltitude += probe.getAltitude();
            avgHumidity += probe.getHumidity();
            avgCo2PPM += probe.getCo2_ppm();
            avgNh4PPM += probe.getNh4_ppm();
            avgToluenePPM += probe.getToluene_ppm();
            avgCoPPM += probe.getCo_ppm();
            avgCh4PPM += probe.getCh4_ppm();
            avgDb += probe.getDb();
        }

        int totalSamples = probeList.size();

        if(avgTemperature!=0){
            avgTemperature /= totalSamples;
        }
        if(avgPressure!=0){
            avgPressure /= totalSamples;
        }
        if(avgCh4PPM!=0){
            avgCh4PPM /= totalSamples;
        }
        if(avgCoPPM!=0){
            avgCoPPM /= totalSamples;
        }
        if(avgToluenePPM!=0){
            avgToluenePPM /= totalSamples;
        }
        if(avgNh4PPM!=0){
            avgNh4PPM /= totalSamples;
        }
        if(avgCo2PPM!=0){
            avgCo2PPM /= totalSamples;
        }
        if(avgHumidity!=0){
            avgHumidity /= totalSamples;
        }
        if(avgAltitude!=0){
            avgAltitude /= totalSamples;
        }
        if(avgDb!=0){
            avgDb /= totalSamples;
        }

        Probe avgProbe = new Probe();
        avgProbe.setTemperature(avgTemperature);
        avgProbe.setPressure(avgPressure);
        avgProbe.setAltitude(avgAltitude);
        avgProbe.setHumidity(avgHumidity);
        avgProbe.setCo_ppm(avgCoPPM);
        avgProbe.setCo2_ppm(avgCo2PPM);
        avgProbe.setCh4_ppm(avgCh4PPM);
        avgProbe.setNh4_ppm(avgNh4PPM);
        avgProbe.setToluene_ppm(avgToluenePPM);
        avgProbe.setDb(avgDb);

        return avgProbe;
    }

    public static class Builder{
        private int id;

        //Station data
        private int stationId;
        private String stationIdentifier;
        private Position position;

        //Sensor data
        private double temperature;
        private double pressure;
        private double altitude;
        private double humidity;
        private double co2_ppm;
        private double nh4_ppm;
        private double toluene_ppm;
        private double co_ppm;
        private double ch4_ppm;
        private double db;

        private Date registeredAt;

        public Builder id(int id){
            this.id = id;
            return this;
        }
        public Builder stationId(int stationId){
            this.stationId = stationId;
            return this;
        }
        public Builder stationIdentifier(String stationIdentifier){
            this.stationIdentifier = stationIdentifier;
            return this;
        }
        public Builder position(Position position){
            this.position = position;
            return this;
        }
        public Builder temperature(double temperature){
            this.temperature = temperature;
            return this;
        }
        public Builder pressure(double pressure) {
            this.pressure = pressure;
            return this;
        }
        public Builder altitude(double altitude) {
            this.altitude = altitude;
            return this;
        }
        public Builder humidity(double humidity) {
            this.humidity = humidity;
            return this;
        }
        public Builder co2_ppm(double co2_ppm) {
            this.co2_ppm = co2_ppm;
            return this;
        }
        public Builder nh4_ppm(double nh4_ppm) {
            this.nh4_ppm = nh4_ppm;
            return this;
        }
        public Builder toluene_ppm(double toluene_ppm) {
            this.toluene_ppm = toluene_ppm;
            return this;
        }
        public Builder co_ppm(double co_ppm) {
            this.co_ppm = co_ppm;
            return this;
        }
        public Builder ch4_ppm(double ch4_ppm) {
            this.ch4_ppm = ch4_ppm;
            return this;
        }
        public Builder decibels(double db) {
            this.db = db;
            return this;
        }
        public Builder registeredAt(Date registeredAt) {
            this.registeredAt = registeredAt;
            return this;
        }

        public Probe build(){
            return new Probe(this);
        }
    }


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getStationId() {
        return stationId;
    }
    public void setStationId(int stationId) {
        this.stationId = stationId;
    }
    public String getStationIdentifier() {
        return stationIdentifier;
    }
    public void setStationIdentifier(String stationIdentifier) {
        this.stationIdentifier = stationIdentifier;
    }
    public Position getPosition() {
        return position;
    }
    public void setPosition(Position position) {
        this.position = position;
    }
    public double getTemperature() {
        return temperature;
    }
    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }
    public double getPressure() {
        return pressure;
    }
    public void setPressure(double pressure) {
        this.pressure = pressure;
    }
    public double getAltitude() {
        return altitude;
    }
    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }
    public double getHumidity() {
        return humidity;
    }
    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }
    public double getCo2_ppm() {
        return co2_ppm;
    }
    public void setCo2_ppm(double co2_ppm) {
        this.co2_ppm = co2_ppm;
    }
    public double getNh4_ppm() {
        return nh4_ppm;
    }
    public void setNh4_ppm(double nh4_ppm) {
        this.nh4_ppm = nh4_ppm;
    }
    public double getToluene_ppm() {
        return toluene_ppm;
    }
    public void setToluene_ppm(double toluene_ppm) {
        this.toluene_ppm = toluene_ppm;
    }
    public double getCo_ppm() {
        return co_ppm;
    }
    public void setCo_ppm(double co_ppm) {
        this.co_ppm = co_ppm;
    }
    public double getCh4_ppm() {
        return ch4_ppm;
    }
    public void setCh4_ppm(double ch4_ppm) {
        this.ch4_ppm = ch4_ppm;
    }
    public double getDb() {
        return db;
    }
    public void setDb(double db) {
        this.db = db;
    }
    public Date getRegisteredAt() {
        return registeredAt;
    }
    public void setRegisteredAt(Date registeredAt) {
        this.registeredAt = registeredAt;
    }

    @Override
    public String toString() {
        return  "TEMPERATURE = " + temperature + " celsius" +
                "\nPRESSURE = " + pressure + " hPa" +
                "\nALTITUDE = " + altitude + " meters" +
                "\nHUMIDITY = " + humidity + "%" +
                "\nCO2_PPM = " + co2_ppm + " ppm" +
                "\nNH4_PPM = " + nh4_ppm + " ppm" +
                "\nTOLUENE_PPM = " + toluene_ppm + " ppm" +
                "\nCO_PPM = " + co_ppm + " ppm" +
                "\nCH4_PPM = " + ch4_ppm + " ppm" +
                "\nDB = " + db + " decibels";
    }
}
