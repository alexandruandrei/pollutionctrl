package ro.pollutionctrl.db;

/**
 * Created by Andrei on 28-Mar-18.
 */
public interface DatabaseConstants {

    String SQLITE_CONNECTION_URL = "jdbc:sqlite:/home/pi/polctrl.db";

    String SQLITE_INSERT_DATA_ST_QUERY = "INSERT " +
            "INTO pctr_probes(station_id, station_identifier, position_lat, " +
            "position_lng, temperature, pressure, altitude, humidity," +
            "co2_ppm, nh4_ppm, toluene_ppm, co_ppm, ch4_ppm, decibels, registered_at) " +
            "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    String SQLITE_INSERT_ACCESSPOINT_QUERY = "INSERT " +
            "INTO pctr_access_points(station_id, timestamp, bssid, signal_level) " +
            "VALUES(?,?,?,?)";

    String SQLITE_INSERT_DATA_NOST_QUERY = "INSERT " +
            "INTO pctr_probes(position_lat, " +
            "position_lng, temperature, pressure, altitude, humidity," +
            "co2_ppm, nh4_ppm, toluene_ppm, co_ppm, ch4_ppm, decibels) " +
            "VALUES(?,?,?,?,?,?,?,?,?,?,?,)";

    int SQLITE_COL_STATION_ID = 1;
    int SQLITE_COL_STATION_IDENTIFIER = 2;
    int SQLITE_COL_POS_LAT = 3;
    int SQLITE_COL_POS_LNG = 4;
    int SQLITE_COL_TEMP = 5;
    int SQLITE_COL_PRESSURE = 6;
    int SQLITE_COL_ALTITUDE = 7;
    int SQLITE_COL_HUMIDITY = 8;
    int SQLITE_COL_CO2 = 9;
    int SQLITE_COL_NH4= 10;
    int SQLITE_COL_TOLUENE = 11;
    int SQLITE_COL_CO = 12;
    int SQLITE_COL_CH4 = 13;
    int SQLITE_COL_DECIBELS= 14;
    int SQLITE_COL_REGISTERED_AT = 15;

    int SQLITE_AP_COL_STATION_ID = 1;
    int SQLITE_AP_COL_TIMESTAMP = 2;
    int SQLITE_AP_BSSID = 3;
    int SQLITE_AP_SIGNAL_LEVEL = 4;

}
