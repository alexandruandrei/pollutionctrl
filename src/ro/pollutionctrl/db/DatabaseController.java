package ro.pollutionctrl.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Andrei on 17-Apr-18.
 */
public class DatabaseController implements DatabaseConstants{

    private Connection connection;
    private static DatabaseController instance;

    private DatabaseController(){
        try{
            connection = DriverManager.getConnection(SQLITE_CONNECTION_URL);
        }catch(SQLException sqle){
            System.out.println("Connection to database could not be established!");
            sqle.printStackTrace();
        }
    }

    public static synchronized DatabaseController getInstance(){
        if(instance == null){
            instance = new DatabaseController();
        }
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }
}
