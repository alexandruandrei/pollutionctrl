package ro.pollutionctrl.db;

import ro.pollutionctrl.data.Probe;
import ro.pollutionctrl.sensors.wifi.AccessPoint;
import ro.pollutionctrl.util.Constants;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Set;

/**
 * Created by Andrei on 17-Apr-18.
 */
public class DatabaseRepository implements DatabaseConstants{

    private DatabaseController dbController;

    public DatabaseRepository(){
        dbController = DatabaseController.getInstance();
    }

    public void registerPollutionProbe(Probe probe, Set<AccessPoint> accessPoints){
        try{
            if(probe.getStationIdentifier() != null && probe.getPosition() != null){
                PreparedStatement preparedStatement = dbController.getConnection().prepareStatement(SQLITE_INSERT_DATA_ST_QUERY);
                preparedStatement.setInt(SQLITE_COL_STATION_ID, probe.getStationId());
                preparedStatement.setString(SQLITE_COL_STATION_IDENTIFIER, probe.getStationIdentifier());
                preparedStatement.setDouble(SQLITE_COL_POS_LAT, probe.getPosition().getLat());
                preparedStatement.setDouble(SQLITE_COL_POS_LNG, probe.getPosition().getLng());
                preparedStatement.setDouble(SQLITE_COL_TEMP, probe.getTemperature());
                preparedStatement.setDouble(SQLITE_COL_PRESSURE, probe.getPressure());
                preparedStatement.setDouble(SQLITE_COL_ALTITUDE, probe.getAltitude());
                preparedStatement.setDouble(SQLITE_COL_HUMIDITY, probe.getHumidity());
                preparedStatement.setDouble(SQLITE_COL_CO2, probe.getCo2_ppm());
                preparedStatement.setDouble(SQLITE_COL_NH4, probe.getNh4_ppm());
                preparedStatement.setDouble(SQLITE_COL_TOLUENE, probe.getToluene_ppm());
                preparedStatement.setDouble(SQLITE_COL_CO, probe.getCo_ppm());
                preparedStatement.setDouble(SQLITE_COL_CH4, probe.getCh4_ppm());
                preparedStatement.setDouble(SQLITE_COL_DECIBELS, probe.getDb());
                preparedStatement.setString(SQLITE_COL_REGISTERED_AT, Constants.PCTR_DATE_FORMAT.format(probe.getRegisteredAt()));
                preparedStatement.executeUpdate();
                preparedStatement.close();

                //register detected access point
                for(AccessPoint ap:accessPoints){
                    registerAccessPoint(ap);
                }
            }
        }catch (SQLException sqle){
            System.out.println("Failed to register probe: " +sqle.getMessage());
            sqle.printStackTrace();
        }
    }

    public void registerAccessPoint(AccessPoint accessPoint) throws SQLException{
        if(accessPoint.getBssid() != null && accessPoint.getSignalLevel() != null){
            PreparedStatement preparedStatement = dbController.getConnection().prepareStatement(SQLITE_INSERT_ACCESSPOINT_QUERY);
            preparedStatement.setInt(SQLITE_AP_COL_STATION_ID, accessPoint.getStationId());
            preparedStatement.setString(SQLITE_AP_COL_TIMESTAMP, Constants.PCTR_DATE_FORMAT.format(accessPoint.getTimestamp()));
            preparedStatement.setString(SQLITE_AP_BSSID, accessPoint.getBssid());
            preparedStatement.setInt(SQLITE_AP_SIGNAL_LEVEL, accessPoint.getSignalLevel());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
    }

}
