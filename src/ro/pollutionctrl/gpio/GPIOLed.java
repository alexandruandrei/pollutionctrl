package ro.pollutionctrl.gpio;

import jdk.dio.*;
import jdk.dio.gpio.*;
import ro.pollutionctrl.gpio.exceptions.GPIOLedException;

import java.io.IOException;

/**
 * Created by Andrei on 10-Mar-18.
 */

public class GPIOLed {

    private boolean turnedOn;
    private int pinNumber;
    private GPIOPin pin;

    public GPIOLed(int pinNumber) throws IOException{
        this.pinNumber = pinNumber;
        init();
    }
    public GPIOLed(int pinNumber, boolean turnedOn) throws IOException{
        this.pinNumber = pinNumber;
        this.turnedOn = turnedOn;
        init();
    }

    private void init() throws IOException{
        GPIOPinConfig pinConfig = new GPIOPinConfig(DeviceConfig.DEFAULT, pinNumber,
                GPIOPinConfig.DIR_OUTPUT_ONLY,
                GPIOPinConfig.MODE_OUTPUT_PUSH_PULL,
                GPIOPinConfig.TRIGGER_NONE,
                false);
        pin = (GPIOPin) DeviceManager.open(GPIOPin.class, pinConfig);
    }
    public void turnOn() throws IOException{
        if(pin!=null){
            pin.setValue(true);
            turnedOn = true;
        }
    }
    public void turnOff() throws IOException{
        if(pin!=null){
            pin.setValue(false);
            turnedOn = false;
        }
    }
    public void close() throws IOException{
        if(pin!=null){
            pin.close();
        }
    }

    /**
     * Blink LED
     * @param amount of times to blink
     * @param interval (millis) between blinks
     */
    public void blink(int amount, int interval) throws IOException, InterruptedException{
        for(int i=0;i<amount;i++){
            turnedOn = !turnedOn;
            pin.setValue(turnedOn);
            Thread.sleep(interval);
        }
        //turn off LED after blink
        turnOff();
    }

    public static void blinkLEDByGPIONumber(int pinNumber) {
        System.out.println("Blinking LED on GPIO pin number " + pinNumber);
        GPIOPin pin = null;
        try {
            GPIOPinConfig pinConfig = new GPIOPinConfig(DeviceConfig.DEFAULT,
                    pinNumber,
                    GPIOPinConfig.DIR_OUTPUT_ONLY,
                    GPIOPinConfig.MODE_OUTPUT_PUSH_PULL,
                    GPIOPinConfig.TRIGGER_NONE,
                    false);
            pin = (GPIOPin)DeviceManager.open(GPIOPin.class, pinConfig);
            boolean pinOn = false;
            //for (int i = 0; i < 20; i++)
            {
                pinOn = !pinOn;
                pin.setValue(pinOn);
                Thread.sleep(1000);
            }
        } catch (IOException ioe) {
            throw new GPIOLedException("IOException while opening device. " +
                    "Make sure you have the appropriate operating system permission to access GPIO devices.", ioe);
        } catch(InterruptedException ie) {
            // ignore
        } finally {
            try {
                System.out.println("Closing GPIO pin number " + pinNumber);
                if (pin != null) {
                    pin.close();
                }
            } catch (Exception e) {
                throw new GPIOLedException("Received exception while trying to close device.", e);
            }
        }
    }

    public boolean isTurnedOn() {
        return turnedOn;
    }
    public void setTurnedOn(boolean turnedOn) {
        this.turnedOn = turnedOn;
    }
    public int getPinNumber() {
        return pinNumber;
    }
    public void setPinNumber(int pinNumber) {
        this.pinNumber = pinNumber;
    }
    public GPIOPin getPin() {
        return pin;
    }
    public void setPin(GPIOPin pin) {
        this.pin = pin;
    }
}
