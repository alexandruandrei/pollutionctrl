package ro.pollutionctrl.gpio;

import java.io.IOException;

/**
 * Created by Andrei on 24-Mar-18.
 */
public class GPIOLedAnim implements LEDConstants{

    public static void startupAnim(){
        try{
            GPIOLed greenLED = new GPIOLed(GREEN_LED_PIN);
            GPIOLed blueLED = new GPIOLed(BLUE_LED_PIN);
            GPIOLed redLED = new GPIOLed(RED_LED_PIN);

            //Close all LEDs before starting
            greenLED.turnOff();
            blueLED.turnOff();
            redLED.turnOff();

            long t= System.currentTimeMillis();
            long end = t+5000;

            int delay = 150;

            while(System.currentTimeMillis() < end) {

                if(!greenLED.isTurnedOn()){
                    greenLED.turnOn();
                }else{
                    greenLED.turnOff();
                }
                Thread.sleep(delay);

                if(!blueLED.isTurnedOn()){
                    blueLED.turnOn();
                }else{
                    blueLED.turnOff();
                }
                Thread.sleep(delay);

                if(!redLED.isTurnedOn()){
                    redLED.turnOn();
                }else{
                    redLED.turnOff();
                }
                Thread.sleep(delay);


                redLED.turnOff();
                blueLED.turnOff();
                greenLED.turnOff();
                Thread.sleep(100);



                if(!redLED.isTurnedOn()){
                    redLED.turnOn();
                }else{
                    redLED.turnOff();
                }
                Thread.sleep(delay);

                if(!blueLED.isTurnedOn()){
                    blueLED.turnOn();
                }else{
                    blueLED.turnOff();
                }
                Thread.sleep(delay);

                if(!greenLED.isTurnedOn()){
                    greenLED.turnOn();
                }else{
                    greenLED.turnOff();
                }
                Thread.sleep(delay);

                redLED.turnOff();
                blueLED.turnOff();
                greenLED.turnOff();
                Thread.sleep(200);
            }

            //Leave only green LED turned on
            if(!greenLED.isTurnedOn()){
                greenLED.turnOff();
            }
            if(blueLED.isTurnedOn()){
                blueLED.turnOff();
            }
            if(redLED.isTurnedOn()){
                redLED.turnOff();
            }

            //Close
            greenLED.close();
            blueLED.close();
            redLED.close();
        }catch (IOException ioe){
            ioe.printStackTrace();
        }catch (InterruptedException ie){
            ie.printStackTrace();
        }
    }

}
