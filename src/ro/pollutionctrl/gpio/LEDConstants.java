package ro.pollutionctrl.gpio;

/**
 * Created by Andrei on 24-Mar-18.
 */
public interface LEDConstants {

    int GREEN_LED_PIN = 21;
    int BLUE_LED_PIN = 12;
    int RED_LED_PIN = 5;
}
