package ro.pollutionctrl.gpio.exceptions;

/**
 * Created by Andrei on 10-Mar-18.
 */
public class GPIOLedException extends RuntimeException{

    public GPIOLedException(String msg, Throwable t) {
        super(msg,t);
    }

}
