package ro.pollutionctrl.main;


import org.eclipse.paho.client.mqttv3.MqttMessage;
import ro.pollutionctrl.data.Position;
import ro.pollutionctrl.data.Probe;
import ro.pollutionctrl.db.DatabaseRepository;
import ro.pollutionctrl.gpio.GPIOLed;
import ro.pollutionctrl.gpio.GPIOLedAnim;
import ro.pollutionctrl.gpio.LEDConstants;
import ro.pollutionctrl.mqtt.MQTTClient;
import ro.pollutionctrl.mqtt.MQTTConstants;
import ro.pollutionctrl.sensors.*;
import ro.pollutionctrl.sensors.wifi.AccessPointReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Andrei on 10-Mar-18.
 */
public class ProgMain {



    /**
     * args[0] = latitude
     * args[1] = longitude
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception{
        System.out.println("Started...");

        int stationId = 1;
        String stationIdentifier = "B8:27:EB:3D:76:B2";
        double MOCK_LAT = Double.parseDouble(args[0]);
        double MOCK_LNG = Double.parseDouble(args[1]);
        Position stationPosition = new Position(MOCK_LAT, MOCK_LNG);

        GPIOLedAnim.startupAnim();

        /** --- LEDs ---**/
        GPIOLed greenLED = null;
        GPIOLed blueLED = null;
        GPIOLed redLED = null;
        greenLED = new GPIOLed(LEDConstants.GREEN_LED_PIN);
        blueLED = new GPIOLed(LEDConstants.BLUE_LED_PIN);
        redLED = new GPIOLed(LEDConstants.RED_LED_PIN);
        greenLED.turnOn();
        blueLED.turnOff();
        redLED.turnOff();

        // Gather data for 15 samples - each 2 seconds
        int NB_SAMPLES = 5;
        int SAMPLE_DELAY = 2000; //2000 millis

        // Initialize sensors
        BMP280 bmp280 = new BMP280(SensorConstants.BMP280_I2C_ADDRESS);
        MQ135 mq135 = new MQ135(SensorConstants.MCP3008_DEVICE, 6);
        MQ9 mq9 = new MQ9(SensorConstants.MCP3008_DEVICE, 7);
        HumiditySensor humiditySensor = new HumiditySensor(SensorConstants.MCP3008_DEVICE, 2);
        MicrophoneMAX9814 microphoneMAX9814 =
                new MicrophoneMAX9814(SensorConstants.MCP3008_DEVICE, 1);

        List<Probe> probeList = new ArrayList<>();

        // Initialize database connection
        DatabaseRepository dbRepository = new DatabaseRepository();

        // Initializa MQTT client
        MQTTClient mqttClient = new MQTTClient("RPI");
        mqttClient.connect();

        while(true){
            if(!probeList.isEmpty()){
                probeList.clear();
            }
            // Gather probes
            System.out.println("Gathering probes ... [" + NB_SAMPLES +
                    "x probes. Gather rate: 1x" + SAMPLE_DELAY/1000 + " seconds]");
            for(int i=0;i<NB_SAMPLES;i++){
                bmp280.read();
                mq135.read();

                Probe probe = new Probe.Builder()
                        .temperature(bmp280.getTemperatureCelsius())
                        .altitude(bmp280.getAltitude())
                        .pressure(bmp280.getPressure())
                        .co2_ppm(mq135.getCO2_PPM())
                        .nh4_ppm(mq135.getNH4_PPM())
                        .toluene_ppm(mq135.getTOLUENE_PPM())
                        .humidity(humiditySensor.getProcessedValue())
                        .co_ppm(mq9.getProcessedValue())
                        .decibels(microphoneMAX9814.getProcessedValue())
                        .build();
                probeList.add(probe);

                System.out.println("Progress: " + (i+1) + "/" + NB_SAMPLES);
                Thread.sleep(SAMPLE_DELAY);
            }

            // Get the average probe
            Probe avgProbe = Probe.averageProbe(probeList);
            if(avgProbe == null){
                break;
            }

            // Set probe station details
            avgProbe.setStationId(stationId);
            avgProbe.setStationIdentifier(stationIdentifier);
            avgProbe.setPosition(stationPosition);
            avgProbe.setRegisteredAt(new Date());

            // Show probe details
            System.out.println("==== Probe ====");
            System.out.println(avgProbe);
            System.out.println("===============");

            // Gather Wi-Fi data
            AccessPointReader accessPointReader = new AccessPointReader();
            accessPointReader.readAccessPoints(avgProbe.getStationId());

            System.out.println("==== Inserting data into sqlite ... ====");
            dbRepository.registerPollutionProbe(avgProbe, accessPointReader.getAccessPointSet());
            System.out.println("=========================================");

            //Modified

            // TODO: Insert probe to db / publish via MQTT
//            MqttMessage payload = new MqttMessage()
//            mqttClient.publish(MQTTConstants.MQTT_PROBE_TOPIC, );
        }

    }

}
