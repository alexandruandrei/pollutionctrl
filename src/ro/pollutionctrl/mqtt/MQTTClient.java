package ro.pollutionctrl.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Created by Andrei on 11-Mar-18.
 * Wrapper for MqttClient Paho
 */
public class MQTTClient implements MQTTConstants{

    private MqttClient mqttClient;
    private String clientID;

    public MQTTClient(String clientID) throws MqttException{
        this.clientID = clientID;
        mqttClient = new MqttClient(MQTT_BROKER, clientID);
    }

    public void connect() throws MqttException{
        mqttClient.connect();
    }

    public void disconnect() throws MqttException{
        mqttClient.disconnect();
    }

    public void publish(String topic, MqttMessage payload) throws MqttException{
        mqttClient.publish(topic, payload);
    }

    public MqttClient getMqttClient() {
        return mqttClient;
    }
    public void setMqttClient(MqttClient mqttClient) {
        this.mqttClient = mqttClient;
    }
    public String getClientID() {
        return clientID;
    }
    public void setClientID(String clientID) {
        this.clientID = clientID;
    }
}
