package ro.pollutionctrl.mqtt;

/**
 * Created by Andrei on 11-Mar-18.
 */
public interface MQTTConstants {

    String MQTT_BROKER_HOST = "tcp://54.154.33.59";
    String MQTT_BROKER_PORT = "1883";
    String MQTT_BROKER = MQTT_BROKER_HOST + ":" + MQTT_BROKER_PORT;

    String MQTT_PROBE_TOPIC = "testtopic1";
}
