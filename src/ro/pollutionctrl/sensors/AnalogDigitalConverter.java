package ro.pollutionctrl.sensors;

import jdk.dio.DeviceManager;
import ro.pollutionctrl.spi.MCP3008;
import ro.pollutionctrl.spi.MCP3008Config;

import java.io.IOException;

/**
 * Created by Andrei on 13-Mar-18.
 * MCP3008 singleton
 */
public class AnalogDigitalConverter {

    private static AnalogDigitalConverter adc;
    private MCP3008 mcp3008;
    private MCP3008Config adcConfig;

    private AnalogDigitalConverter(int deviceNumber){
        try{
            adcConfig = new MCP3008Config(deviceNumber);
            mcp3008 = (MCP3008) DeviceManager.open(adcConfig);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public static AnalogDigitalConverter getInstance(int deviceNumber){
        synchronized (AnalogDigitalConverter.class){
            if(adc == null){
                adc = new AnalogDigitalConverter(deviceNumber);
                return adc;
            }else{
                return adc;
            }
        }
    }

    public void close() throws IOException {
        adc.mcp3008.close();
    }
    public int readChannel(int channel){
        return adc.mcp3008.readChannel(channel);
    }
    public boolean isOpen(){
        return adc.mcp3008.isOpen();
    }

    public MCP3008 getMcp3008() {
        return mcp3008;
    }
    public MCP3008Config getAdcConfig() {
        return adcConfig;
    }
    public void setAdcConfig(MCP3008Config adcConfig) {
        this.adcConfig = adcConfig;
    }

}
