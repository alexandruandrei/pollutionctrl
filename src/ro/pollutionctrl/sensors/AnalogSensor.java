package ro.pollutionctrl.sensors;

import jdk.dio.DeviceManager;
import ro.pollutionctrl.spi.MCP3008;
import ro.pollutionctrl.spi.MCP3008Config;

import java.io.IOException;

/**
 * Created by Andrei on 10-Mar-18.
 * Used to interface with analog sensors attached to MCP3008
 */
public abstract class AnalogSensor implements AnalogSensorOperation {

    private AnalogDigitalConverter analogDigitalConverter;
    private int deviceNumber;
    private int analogChannel;
    private double rawValue;

    public AnalogSensor(int deviceNumber, int analogChannel){
        this.analogChannel = analogChannel;
        this.deviceNumber = deviceNumber;
        init();
    }

    private void init(){
        try{
            analogDigitalConverter = AnalogDigitalConverter.getInstance(deviceNumber);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void closeADC() throws IOException{
        analogDigitalConverter.close();
    }
    public double getRawValue() {
        return analogDigitalConverter.readChannel(analogChannel);
    }
    public int getDeviceNumber() {
        return deviceNumber;
    }
    public void setDeviceNumber(int deviceNumber) {
        this.deviceNumber = deviceNumber;
    }
    public int getAnalogChannel() {
        return analogChannel;
    }
    public void setAnalogChannel(int analogChannel) {
        this.analogChannel = analogChannel;
    }
    public AnalogDigitalConverter getAnalogDigitalConverter() {
        return analogDigitalConverter;
    }
    public void setAnalogDigitalConverter(AnalogDigitalConverter analogDigitalConverter) {
        this.analogDigitalConverter = analogDigitalConverter;
    }
}
