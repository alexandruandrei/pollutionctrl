package ro.pollutionctrl.sensors;

/**
 * Created by Andrei on 10-Mar-18.
 */
public interface AnalogSensorOperation {

    double getProcessedValue();

}
