package ro.pollutionctrl.sensors;

/**
 * Created by Andrei on 13-Mar-18.
 */
public class ButtonSensor extends AnalogSensor {

    public ButtonSensor(int deviceNumber, int analogChannel) {
        super(deviceNumber, analogChannel);
    }

    @Override
    public double getProcessedValue() {
        return getRawValue();
    }
}
