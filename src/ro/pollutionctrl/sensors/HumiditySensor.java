package ro.pollutionctrl.sensors;

/**
 * Created by Andrei on 13-Mar-18.
 * Humidity sensor SYH-2R
 */
public class HumiditySensor extends AnalogSensor {

    public HumiditySensor(int deviceNumber, int analogChannel) {
        super(deviceNumber, analogChannel);
    }

    /**
     * Returns relative humidity % (RH) level
     * @return RH
     */
    @Override
    public double getProcessedValue() {
        double humidityLevel;
        double rawVal = getRawValue();

        double ADC_MIN = 0;
        double ADC_MAX = 1023;
        double RH_MIN = 20;
        double RH_MAX = 95;

        humidityLevel = (((rawVal - ADC_MIN) * (RH_MAX - RH_MIN)) / (ADC_MAX - ADC_MIN)) + RH_MIN;

        return humidityLevel;
    }

}
