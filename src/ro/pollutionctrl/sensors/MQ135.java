package ro.pollutionctrl.sensors;


/**
 * Created by Andrei on 10-Mar-18.
 * Used to interface with SNS-MQ135 sensor - analog output
 */
public class MQ135 extends AnalogSensor implements SensorConstants{

    private double CO2_PPM;
    private double NH4_PPM;
    private double TOLUENE_PPM;

    public MQ135(int deviceNumber, int analogChannel) {
        super(deviceNumber, analogChannel);
    }

    public void read(){
        int RL = 10;
        double R0 = 76.63d;
        double vOut = getRawValue()*3.3/4095.0d;
        double rs = ((5.0d*RL) - (RL*vOut));
        double ratio = rs/R0;

        this.CO2_PPM = 116.6020682 * Math.pow(ratio, -2.769034857);
        this.NH4_PPM = 102.694 * Math.pow(ratio, -2.48818);
        this.TOLUENE_PPM = 43.7748 * Math.pow(ratio, -3.42936);
    }

    @Override
    public double getProcessedValue() {
        this.read();
        return CO2_PPM;
    }


    public double getCO2_PPM() {
        return CO2_PPM;
    }

    public void setCO2_PPM(double CO2_PPM) {
        this.CO2_PPM = CO2_PPM;
    }

    public double getNH4_PPM() {
        return NH4_PPM;
    }

    public void setNH4_PPM(double NH4_PPM) {
        this.NH4_PPM = NH4_PPM;
    }

    public double getTOLUENE_PPM() {
        return TOLUENE_PPM;
    }

    public void setTOLUENE_PPM(double TOLUENE_PPM) {
        this.TOLUENE_PPM = TOLUENE_PPM;
    }

    /**
     * Get the correction factor to correct for temperature and humidity
     * @param t Ambient air temperature
     * @param h Relative humidity
     * @return Calculated correction factor
     */
    private double getCorrectionFactor(double t, double h){
        return MQ135_CORA * t * t - MQ135_CORB * t
                + MQ135_CORC - (h - 33.) * MQ135_CORD;
    }
    /**
     * Get the resistance of the sensor
     * @return sensor resistance in kOhm
     */
    private double getResistance(double val){
        return ((1023./val) - 1.) * MQ135_RLOAD;
    }
    /**
     * Get the resistance of sensor corrected for temperature/humidity
     * @param t Ambient air temperature
     * @param h Relative humidity
     * @return corrected sensor resistance in kOhm
     */
    private double getCorrectedResistance(double t, double h){
        return getResistance(getRawValue())/getCorrectionFactor(t, h);
    }
    /**
     * Get the ppm of CO2 detected
     * @return ppm of CO2
     */
    private double getPPM(double rawValue){
        return MQ135_PARA * Math.pow((getResistance(rawValue)/MQ135_RZERO), -MQ135_PARAB);
    }
    private double getCorrectedPPM(float t, float h){
        return MQ135_PARA * Math.pow((getCorrectedResistance(t, h)/MQ135_RZERO),
                -MQ135_PARAB);
    }
    public double getRZERO(double rawValue){
        return getResistance(rawValue) * Math.pow((MQ135_ATMCO2/MQ135_PARA), (1./MQ135_PARAB));
    }
    public double getCorrectedRZero(float t, float h){
        return getCorrectedResistance(t, h) * Math.pow((MQ135_ATMCO2/MQ135_PARA),
                (1./MQ135_PARAB));
    }

}
