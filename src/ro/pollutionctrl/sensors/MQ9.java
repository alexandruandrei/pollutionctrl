package ro.pollutionctrl.sensors;

/**
 * Created by Andrei on 25-Mar-18.
 */
public class MQ9 extends AnalogSensor{

    public MQ9(int deviceNumber, int analogChannel) {
        super(deviceNumber, analogChannel);
    }

    @Override
    public double getProcessedValue() {
        int RL = 10;
        double R0 = 76.63d;
        double vOut = getRawValue()*3.3/4095.0d;
        double rs = ((5.0d*RL) - (RL*vOut));
        double ratio = rs/R0;
        double ppm = (double)620.028/Math.pow(ratio, (2000/1039));
        return ppm;
    }
}
