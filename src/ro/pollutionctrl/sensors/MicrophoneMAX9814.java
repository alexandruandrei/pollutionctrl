package ro.pollutionctrl.sensors;


/**
 * Created by Andrei on 26-Mar-18.
 */
public class MicrophoneMAX9814 extends AnalogSensor{

    public MicrophoneMAX9814(int deviceNumber, int analogChannel) {
        super(deviceNumber, analogChannel);
    }

    @Override
    public double getProcessedValue(){
        return getRawValue();
    }
}
