package ro.pollutionctrl.sensors;

/**
 * Created by Andrei on 10-Mar-18.
 */
public interface SensorConstants {

    //--------------------  SPI  --------------------
    int MCP3008_DEVICE = 300; //device number for mcp3008
    double MQ135_RLOAD = 10.0; //the load resistance on the board
    double MQ135_RZERO = 76.63; //calibration resistance at atmospheric CO2 level
    //Parameters for calculating ppm of CO2 from sensor resistance
    double MQ135_PARA = 116.6020682;
    double MQ135_PARAB = 2.769034857;
    //Parameters to model temperature and humidity dependence
    double MQ135_CORA = 0.00035;
    double MQ135_CORB = 0.02718;
    double MQ135_CORC = 1.39538;
    double MQ135_CORD = 0.0018;
    //Atmospheric CO2 level for calibration purposes
    double MQ135_ATMCO2 = 397.13;

    //--------------------  I2C  --------------------
    int BMP280_I2C_ADDRESS = 0x77;
    double BMP280_SEALEVEL_PRESSURE = 1013.25d;
}
