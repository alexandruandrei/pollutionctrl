package ro.pollutionctrl.sensors;

/**
 * Created by Andrei on 12-Mar-18.
 */
public class TemperatureSensor extends AnalogSensor {

    public TemperatureSensor(int deviceNumber, int analogChannel) {
        super(deviceNumber, analogChannel);
    }

    @Override
    public double getProcessedValue() {
        double rawValue = getRawValue();
        double voltage = rawValue * 3.3;
        voltage /= 1024.0d;
        double tempCelsius = (voltage-0.5d)*100;
        return tempCelsius;
    }
}
