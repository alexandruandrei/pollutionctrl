package ro.pollutionctrl.sensors.wifi;

import java.util.Date;

/**
 * Created by Andrei on 15-Apr-18.
 */
public class AccessPoint {

    private int stationId;
    private String bssid;
    private String essid;
    private Integer signalLevel;
    private Date timestamp;

    public AccessPoint(){

    }

    public AccessPoint(int stationId, String bssid, String essid, Integer signalLevel, Date timestamp){
        this.stationId = stationId;
        this.essid = essid;
        this.bssid = bssid;
        this.signalLevel = signalLevel;
        this.timestamp = timestamp;
    }

    public AccessPoint(String bssid, String essid, Integer signalLevel){
        this.bssid = bssid;
        this.essid = essid;
        this.signalLevel = signalLevel;
    }

    public String getEssid() {
        return essid;
    }
    public void setEssid(String essid) {
        this.essid = essid;
    }
    public Integer getSignalLevel() {
        return signalLevel;
    }
    public void setSignalLevel(Integer signalLevel) {
        this.signalLevel = signalLevel;
    }
    public String getBssid() {
        return bssid;
    }
    public void setBssid(String bssid) {
        this.bssid = bssid;
    }
    public int getStationId() {
        return stationId;
    }
    public void setStationId(int stationId) {
        this.stationId = stationId;
    }
    public Date getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccessPoint that = (AccessPoint) o;

        if (bssid != null ? !bssid.equals(that.bssid) : that.bssid != null) return false;
        return essid != null ? essid.equals(that.essid) : that.essid == null;
    }

    @Override
    public int hashCode() {
        int result = bssid != null ? bssid.hashCode() : 0;
        result = 31 * result + (essid != null ? essid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AccessPoint{" +
                "stationId=" + stationId +
                ", bssid='" + bssid + '\'' +
                ", essid='" + essid + '\'' +
                ", signalLevel=" + signalLevel +
                ", timestamp=" + timestamp +
                '}';
    }
}
