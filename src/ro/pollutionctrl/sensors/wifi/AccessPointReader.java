package ro.pollutionctrl.sensors.wifi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Andrei on 15-Apr-18.
 */
public class AccessPointReader {

    private Set<AccessPoint> accessPointSet;

    public synchronized void readAccessPoints(int stationId) throws IOException{
        long startTime = System.currentTimeMillis();
        System.out.println("Reading access points list...");
        Runtime runtime = Runtime.getRuntime();
        String getWListCommand = "sudo iwlist wlan0 scan";
        Process process = runtime.exec(getWListCommand);

        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(process.getInputStream()));

        String line = null;
        String bssid = null;
        String essid = null;
        String signalLevel = null;

        accessPointSet = new HashSet<>();
        while((line = bufferedReader.readLine()) != null ){
            int essidIndex = line.indexOf("ESSID:");
            if(essidIndex != -1){
                essid = line.substring(essidIndex + "ESSID:".length());
                essid = essid.trim();
            }else{
                int signalIndex = line.indexOf("Signal level=");
                if(signalIndex != -1){
                    signalLevel = line.substring(signalIndex + "Signal level=".length());
                    signalLevel = signalLevel.trim();
                }

                int bssidIndex = line.indexOf("Address:");
                if(bssidIndex != -1){
                    bssid = line.substring(21 + "Address:".length());
                    bssid = bssid.trim();
                }
            }
            if(essid != null && signalLevel != null){
            signalLevel = signalLevel.replace(" dBm", "");
                AccessPoint ap = new AccessPoint(stationId, bssid, essid, Integer.parseInt(signalLevel), new Date());
                accessPointSet.add(ap);
            }
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Done reading access points list.");
        System.out.println("Elapsed time: " + (endTime - startTime) + "milis");

        for(AccessPoint ap:accessPointSet){
            System.out.println(ap);
        }
    }

    public Set<AccessPoint> getAccessPointSet() {
        return accessPointSet;
    }
    public void setAccessPointSet(Set<AccessPoint> accessPointSet) {
        this.accessPointSet = accessPointSet;
    }
}
