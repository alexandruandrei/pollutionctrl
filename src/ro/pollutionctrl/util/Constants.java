package ro.pollutionctrl.util;

import java.text.SimpleDateFormat;

/**
 * Created by Andrei on 17-Apr-18.
 */
public interface Constants {

    String PCTR_DATE_FORMAT_STR = "dd/MM/yyyy HH:mm:ss";
    SimpleDateFormat PCTR_DATE_FORMAT = new SimpleDateFormat(PCTR_DATE_FORMAT_STR);


}
